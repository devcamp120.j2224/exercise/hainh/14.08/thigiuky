package com.example.demo.Model;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "images")
public class Image {
 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    @NotEmpty(message = "không được để trống tên ảnh")
    private String tenAnh ;

    @NotEmpty(message = "không được để trống link ảnh")
    private String linkAnh ;

    @Length(min = 3 , max = 100 , message = "mô tả tối đa 200 ký tự")
    private String moTa ;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @JsonFormat(pattern = "MM/dd/yyyy")
    private Date ngayTao ;

    @ManyToOne
    @JoinColumn(name = "album_id")
    @JsonBackReference
    private Album album ;


    @Transient
    private String albumName;
    
    @JsonIgnore
    public String getAlbumName() {
        return getAlbum().getTenAlbum();
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    
    public Image() {
    }

    public Image(Long id,String tenAnh,String linkAnh,String moTa, Date ngayTao, Album album) {
        this.id = id;
        this.tenAnh = tenAnh;
        this.linkAnh = linkAnh;
        this.moTa = moTa;
        this.ngayTao = ngayTao;
        this.album = album;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTenAnh() {
        return tenAnh;
    }

    public void setTenAnh(String tenAnh) {
        this.tenAnh = tenAnh;
    }

    public String getLinkAnh() {
        return linkAnh;
    }

    public void setLinkAnh(String linkAnh) {
        this.linkAnh = linkAnh;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }


}
