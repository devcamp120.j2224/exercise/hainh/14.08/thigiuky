package com.example.demo.Model;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "albums")
public class Album {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;


    @NotEmpty(message = "không được để trống mã album" )
    @Length(min = 3, max = 10 , message = "ít nhất 3 ký tự , tối đa là 10") 
    private String maAlbum ; 

    
    @NotEmpty(message = "không được để trống tên album")
    private String tenAlbum ;

    @Length(min = 3 , max = 100 , message = "tối đa 200 ký tự")
    private String moTa ; 

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @JsonFormat(pattern = "MM/dd/yyyy")
    private Date ngayTao ;

    @OneToMany(mappedBy = "album", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Image> imagesss ;


    public Album() {
    }

    

    public Album(Long id, String maAlbum, String tenAlbum, String moTa, Date ngayTao) {
        this.id = id;
        this.maAlbum = maAlbum;
        this.tenAlbum = tenAlbum;
        this.moTa = moTa;
        this.ngayTao = ngayTao;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMaAlbum() {
        return maAlbum;
    }

    public void setMaAlbum(String maAlbum) {
        this.maAlbum = maAlbum;
    }

    public String getTenAlbum() {
        return tenAlbum;
    }

    public void setTenAlbum(String tenAlbum) {
        this.tenAlbum = tenAlbum;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }



    public Set<Image> getImagesss() {
        return imagesss;
    }



    public void setImagesss(Set<Image> imagesss) {
        this.imagesss = imagesss;
    }

    
}
