package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Image;

public interface iImageRespository extends JpaRepository <Image , Long> {
    boolean existsImagesssByTenAnh(String name) ;
}
