package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Album;

public interface iAlbumRespository  extends JpaRepository <Album , Long>{
    boolean existsAlbumsByMaAlbum(String name) ;
}
