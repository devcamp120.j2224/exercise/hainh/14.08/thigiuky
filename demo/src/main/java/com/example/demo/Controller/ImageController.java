package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Album;
import com.example.demo.Model.Image;
import com.example.demo.Respository.iAlbumRespository;
import com.example.demo.Respository.iImageRespository;

@RestController
@CrossOrigin
@RequestMapping("image")
public class ImageController {

    @Autowired
    iImageRespository iImageRespository;

    @Autowired
    iAlbumRespository iAlbumRespository;

    // lấy danh sách all image
    @GetMapping("/all")
    public ResponseEntity<Object> getAllImage() {
        List<Image> imageList = new ArrayList<Image>();
        try {

            iImageRespository.findAll().forEach(orderElement -> {
                imageList.add(orderElement);
            });
            return new ResponseEntity<Object>(imageList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    // tìm image bởi id
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getImageById(@Valid @PathVariable(name = "id") Long id) {
        Optional<Image> order = iImageRespository.findById(id);
        if (order.isPresent()) {
            return new ResponseEntity<Object>(order, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    // tạo image by album id
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createImage(@PathVariable("id") Long id, @Valid @RequestBody Image image) {
        Optional<Album> albumData = iAlbumRespository.findById(id);

        if (albumData.isPresent()) {
            Image newImage = new Image();
            newImage.setLinkAnh(image.getLinkAnh());
            newImage.setMoTa(image.getMoTa());
            newImage.setTenAnh(image.getTenAnh());
            newImage.setNgayTao(new Date());

            Album album = albumData.get();
            newImage.setAlbum(album);
            newImage.setAlbumName(album.getTenAlbum());

            if (iImageRespository.existsImagesssByTenAnh(image.getTenAnh())) {

                return new ResponseEntity<Object>(  "timestamp : " + new Date()             + "\n" + "\n" +
                                                    "errors: [ "                            + "\n" + "\n" +
                                                    "tên ảnh đã tồn tại"                    + "\n" + "\n" +
                                                    "]"                                     + "\n" + "\n" +
                                                    "status: 409 ", HttpStatus.CONFLICT);

            }

            Image savedRole = iImageRespository.save(newImage);
            try {
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            } catch (Exception e) {
                System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(new Date() + "\n" +
                    " : không tìm thấy id của albums  " + "\n" +
                    " : có thể server đang có vấn đề , thử lại trong giây lát " + "\n" +
                    " : trong lúc chờ vui lòng xem lại quá trình tạo mới image của bạn ", HttpStatus.NOT_FOUND);
        }

    }

    // update image by image id
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateImage(@PathVariable(name = "id") Long id,
            @Valid @RequestBody Image imageUpdate) {
        Optional<Image> imageData = iImageRespository.findById(id);

        if (imageData.isPresent()) {

            Image image = imageData.get();
            image.setTenAnh(imageUpdate.getTenAnh());
            image.setLinkAnh(imageUpdate.getLinkAnh());
            image.setMoTa(imageUpdate.getMoTa());
            image.setNgayTao(imageUpdate.getNgayTao());

            if (iImageRespository.existsImagesssByTenAnh(image.getTenAnh())) {

                return new ResponseEntity<Object>(  "timestamp : " + new Date()             + "\n" + "\n" +
                                                    "errors: [ "                            + "\n" + "\n" +
                                                    "tên ảnh đã tồn tại"                    + "\n" + "\n" +
                                                    "]"                                     + "\n" + "\n" +
                                                    "status: 409 ", HttpStatus.CONFLICT);

            }
            iImageRespository.save(image);
            try {
                return new ResponseEntity<>(image, HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(new Date() + "\n" +
                    " : không tìm thấy id của albums  " + "\n" +
                    " : có thể server đang có vấn đề , thử lại trong giây lát " + "\n" +
                    " : trong lúc chờ vui lòng xem lại quá trình tạo mới image của bạn ", HttpStatus.NOT_FOUND);
        }
    }

    // delete image bởi id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteImageById(@PathVariable Long id) {
        try {
            iImageRespository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
        }
    }
}
