package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Album;
import com.example.demo.Respository.iAlbumRespository;
import com.example.demo.Validate.CustomGlobalExceptionHandler;

@RestController
@CrossOrigin
@RequestMapping("album")
public class AlbumController {
    
    @Autowired
    iAlbumRespository iAlbumRespository;

    @Autowired
    CustomGlobalExceptionHandler customGlobal ;

    // lấy danh sách all album
    @GetMapping("/all")
    public ResponseEntity<Object> getAllAlbum() {
        List<Album> albumList = new ArrayList<Album> ();
       
        iAlbumRespository.findAll().forEach(userElement ->{
            albumList.add(userElement);
        });
        if (!albumList.isEmpty())
            return new ResponseEntity<Object>(albumList, HttpStatus.OK);
        else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    // tìm album by id
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getAlbumById(@Valid @PathVariable(name = "id", required = true) Long id) {
        Optional<Album> albumFounded = iAlbumRespository.findById(id);
        if (albumFounded.isPresent())
            return new ResponseEntity<Object>(albumFounded, HttpStatus.OK);
        else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

       // tạo album
    @PostMapping("/create")
    public ResponseEntity<Object> createALbum(@Valid @RequestBody Album album) {
        try {
                 Album newAlbum = new Album();
                 newAlbum.setMaAlbum(album.getMaAlbum());
                 newAlbum.setTenAlbum(album.getTenAlbum());
                 newAlbum.setMoTa(album.getMoTa());
                 newAlbum.setNgayTao(new Date());
                 newAlbum.setImagesss(album.getImagesss());

                 if(iAlbumRespository.existsAlbumsByMaAlbum(album.getMaAlbum())){

                    return new ResponseEntity<Object>(  "timestamp : " + new Date()         + "\n"  + "\n" +
                                                        "errors: [ "                        + "\n"  + "\n" +  
                                                                    "mã album đã tồn tại"   + "\n"  + "\n" + 
                                                                "]"                         + "\n"  + "\n" + 
                                                        "status: 409 " , HttpStatus.CONFLICT);

                 }
                 iAlbumRespository.save(newAlbum); 
                
                    return new ResponseEntity<Object>(newAlbum, HttpStatus.CREATED);
             } 
             catch (Exception e) {
                return ResponseEntity.unprocessableEntity().body("tạo album thất bại , xem lại quá trình tạo mới của bạn"+ e.getCause().getCause().getMessage());
            }
        }     
    



    //update album bởi id
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updatelAlbum(  @PathVariable (name ="id") Long id, @Valid @RequestBody Album albumUpdate) {
        Optional<Album> albumData = iAlbumRespository.findById(id);

        if (albumData.isPresent()){

            Album album = albumData.get();
            album.setMaAlbum(albumUpdate.getMaAlbum());
            album.setTenAlbum(albumUpdate.getTenAlbum());
            album.setMoTa(albumUpdate.getMoTa()) ;
            album.setImagesss(albumUpdate.getImagesss());
            album.setNgayTao(albumUpdate.getNgayTao());

            
        if(iAlbumRespository.existsAlbumsByMaAlbum(albumUpdate.getMaAlbum())){

            return new ResponseEntity<Object>(  "timestamp : " + new Date()             + "\n"  + "\n" +
                                                    "errors: [ "                        + "\n"  + "\n" +  
                                                                "mã album đã tồn tại"   + "\n"  + "\n" + 
                                                            "]"                         + "\n"  + "\n" + 
                                                    "status: 409 " , HttpStatus.CONFLICT);

        }

                 iAlbumRespository.save(album);
            try {
                return ResponseEntity.ok(album);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Can not execute operation of this Entity"+ e.getCause().getCause().getMessage());
            }
        } 

        else {
            return new ResponseEntity<Object> ("ko update được , xem lại quá trình update của bạn ", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }         
    
    // delete album bởi id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deletelAlbum(@PathVariable("id") Long id){
        Optional<Album> albumData = iAlbumRespository.findById(id) ;
        if (albumData.isPresent()) {
            try {
                iAlbumRespository.deleteById(id);
                return new ResponseEntity<Object>(HttpStatus.NO_CONTENT) ;
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>("User not found! ", HttpStatus.NOT_FOUND);
        }
    } 
}
